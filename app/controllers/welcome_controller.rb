class WelcomeController < ApplicationController
  before_filter :find_action, :only => [:add]
  layout 'standard'
  def index
    @note_list = Stickynote.order("created ASC")
  end

  def add
    @sticky = Stickynote.new
    @sticky.created = DateTime.now
    @sticky.save
    render :json => {:response => true, :new_note => @sticky.id}
  end

  def remove
    @sticky = Stickynote.destroy(params[:id])
    render :json => {:response => true}
  end

  def update
    @sticky = Stickynote.find(params[:id])
    @sticky.note = params[:content]
    @sticky.save
    render :json => {:response => true}
  end

  private
  def find_action
  end
end