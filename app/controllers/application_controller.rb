class ApplicationController < ActionController::Base
  #helper :all
  protect_from_forgery

  def ispost
    return request.request_method_symbol == :post
  end

  def isget
    return request.request_method_symbol == :get
  end
end
