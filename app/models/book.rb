class Book < ActiveRecord::Base
  attr_protected
 #attr_accessible :title, :description, :price, :email, :is_active, :category_id  
 
  #has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  
 validates_length_of :title, :description, :within => 1..20, :message => 'is too short'
 
 validates :email, :format => {:with => /^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/, :message => 'Please enter an valid email'} 
 
end
