class CreateTableStickynotes < ActiveRecord::Migration
  def up
    create_table :stickynotes do |t|
      t.column :note, :string, :limit => 20000, :null => true
      t.column :created, :timestamp
    end
  end

  def down
    drop_table :stickynotes
  end
end
